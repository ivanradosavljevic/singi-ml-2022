# Vežbe 2
*Cilj vežbi: Rad sa slikama, učitavanje slika i njihova obrada.*

1. Napraviti funkcije za učitavanje slika u pgm i ppm formatima. Slike učitati kao numpy matrice.
2. Prikazati učitane slike.
3. Napraviti funkcije za učitavanje slika iza MNIST skupa podataka. Slike učitati kao numpy matrice.
4. Prikazati jednu pored druge 5 nasumično odabranih slika iz MNIST skupa podataka.
5. Napraviti funkciju za računanje statističkih podataka o crnobelim slikama. Statistika treba da uključuje podatak o minimalnom i maksimalnom intenzitetu piksela, prosečnoj vrednosti piksela i histogram intenziteta piksela.
6. Grafički prikazati histogram.
7. Proširiti prethodni zadatak tako da radi na slikama u boji.
8. Napraviti funkciju koja iseca deo slike i na osnovu odsečenog dela pravi novu sliku.
9. Napraviti funkciju koja na sliku dodaje nasumičan zrnasti šum.
10. Napraviti funkciju koja sliku prikazuje u ogledalu po vertikalnoj ili po horizontalnoj osi.
11. Napraviti funkciju koja sliku skalira za zadate koeficijente po x i y osi.
12. Napraviti funkciju koja sliku rotira oko centra slike za proizvoljno zadati ugao.
13. Napraviti funkciju koja sliku deformiše po dužini ili širini tako što je isteže po zadatoj osi za n piksela.
14. Napraviti funkciju koja sa slike uklanja zrnasti šum.
15. Napraviti funkciju koja zamućuje proizvoljnu ulaznu sliku. Omogućiti da korisnik bira jačinu zamućenja.
16. Napraviti funkciju koja sa zadate slike izdvaja sve ivice. Izdvojene ivice prikazati na novoj slici.